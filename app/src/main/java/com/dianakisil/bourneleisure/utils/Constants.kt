package com.dianakisil.bourneleisure.utils

/** The base URL of the API */
const val BASE_URL: String = "https://haven-tech-test.s3-eu-west-1.amazonaws.com/tech+test+json.json"