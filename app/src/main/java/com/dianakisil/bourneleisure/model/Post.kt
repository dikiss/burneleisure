package com.dianakisil.bourneleisure.model

/**
 * Class which provides a model for post
 * @constructor Sets all properties of the post
 * @property familyid
 * @property groupsize
 * @property caravan
 */
data class Post(val familyid: Int, val groupsize: Int, val caravan: Int)